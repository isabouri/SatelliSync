"""
Author: Ivan Sabourin
"""

import sys

def get_events(source:str) -> str:
    """
    From the contents of a VCS file, return only the VEVENTs

    source: text content of the VCS file

    output: only the VEVENTs form the input
    """
    lines = source.split("\n")
    events = []

    listening = False
    for line in lines:
        if listening:
            events[-1].append(line)
            if line == "END:VEVENT":
                listening = False
        else:
            if line == "BEGIN:VEVENT":
                listening = True
                events.append([])
                events[-1].append(line)

    events = "\n".join(["\n".join(i) for i in events])
    return events

def get_prefix(source:str) -> str:
    """
    Returns the first lines of the vcal beofre the events

    source: text content of the VCS file

    output: every line before the first potential event
    """
    lines = source.split("\n")
    res = lines[:4]
    res = "\n".join(res)
    return res

def merge(week1:str, week2:str)->str:
    """
    merges the events of two whole VCS files and turns them into a valid VCS file

    week1: text content of the first file
    
    week2: text content of the second file

    output: valid VCS file containing the events from both files
    """
    res = [get_prefix(week1)]

    for week in [week1, week2]:
        if "<title> - Erreur 404 - </title>" in week:
            raise RuntimeError("404 détecté dans le retour de GPU.")
        if "BEGIN:VEVENT" in week:
            events = get_events(week)
            res.append(events)
    res.append("END:VCALENDAR")
    res = "\n".join(res)
    return res
