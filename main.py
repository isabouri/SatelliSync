"""
Author: Ivan Sabourin
"""

from bottle import route, run, template, HTTPResponse, static_file, error
import EDTFetcher

@error(404)
def error404(error):
    return "<h1>Page introuvable</h1><a href=\"/\">Retour à l'accueil</a>"

@route('/')
def index():
    return template("""
    <h1>Obtenir l'URL permanante pour mon emploi du temps GPU</h1>
    <div style="display:flex;flex-direction:row">
        <label for="student_id">Numéro d'étudiant (6 chiffres) : </label>
        <input type="text" id="student_id" name="student_id" required minlength="6" maxlength="6" size="7" autocomplete="Numéro" style="margin: 0 5"/>
        <button id="confirmButton">Confirmer</button>
    </div>
    <p><a href="https://forge.iut-larochelle.fr/isabouri/SatelliSync/-/wikis/home">Comment ça marche ?</a></p>
    <div style="display:flex;flex-direction:row">
        <h2>Version d'affichage</h2>
    </div>
    <label for="version">Version:</label>
    <select name="version" id="version">
        <option value="latest">Rester à jour - Recommandé (1.2.0)</option>
        <option value="1.2.0">1.2.0</option>
        <option value="1.1.0">1.1.0</option>
        <option value="1.0.0">1.0.0</option>
    </select>
    <div style="position:absolute;bottom:0;">
        <p>Le code source est disponible sur <a href="https://forge.iut-larochelle.fr/isabouri/SatelliSync">le Gitlab de l'IUT de La Rochelle</a></p>
        <p>Ce programme est distribué sous <a href="https://www.gnu.org/licenses/agpl-3.0.fr.html">AGPLv3</a></p>
    </div>
    <script>
        function openURL() {
            let studentId = document.getElementById("student_id").value;
            if (studentId.length === 6)
            {
                let e = document.getElementById("version");
                let value = e.value;
                if (value === "latest")
                {
                    window.location.href = "/" + studentId;
                }
                else
                {
                    window.location.href = "/" + studentId + "/" + value;
                }
            }
            else
            {
                alert("Veuillez entrer un numéro d'étudiant valide (6 chiffres).");
            }
        }

        document.getElementById("confirmButton").addEventListener("click", openURL);

        document.getElementById("student_id").addEventListener("keyup", function(event) {
            if (event.key === "Enter")
            {
                openURL();
            }
        });
    </script>
    """)

@route('/favicon.ico')
def index():
    return static_file('favicon.ico', root='.')

@route('/<student_id>') # Default: latest
@route('/<student_id>/<version>') # Specify version
def index(student_id:str, version:str="latest"):
    """
    It is very important to check the data passed in parameters against injection.
    First, we check student_id because it is easy to check (convert to int). If this fails, then we return a static error.
    If the check succeeds, we know it is safe to include it in fstrings.
    The version is checked afterwards: when it is wrong, student_id is included in the html being returned
    We don't want someone to create and share links like /noversion/"<%2Fa><script>evilCode();<%2Fscript>
    We haven't been able to actually create that attack because bottle interprets %2F as / so it just doesn't find a route but better safe than sorry
    """
    # 1 : Check student id
    try:
        student_id = int(student_id)
    except ValueError as e:
        print(e)
        return HTTPResponse(status=400, body="Erreur dans les données. Vérifiez le numéro d'étudiant.")

    # 2 : Check version
    supported_versions = ["1.0.0", "1.0.1", "1.1.0", "1.2.0"]
    if version == "latest":
        version = supported_versions[-1]
    try:
        assert version in supported_versions
    except AssertionError as e:
        print(e)
        return HTTPResponse(status=404, body=f"Le numéro de version n'est pas reconnu. Essayez <a href=\"/{student_id}\">ce lien</a> à la place pour utiliser la version {supported_versions[-1]}.")
    
    # 3 : Get timetable
    try:
        student_id = int(student_id)
        EDTFetcher.assertNumber(student_id, 999999)
        edt_data = EDTFetcher.getEDT(student_id, version)

        response = HTTPResponse(status=200, body=edt_data)
        response.set_header('Content-Type', 'text/plain')
        return response
    except AssertionError as e:
        print(e)
        return HTTPResponse(status=400, body="Erreur dans les données. Vérifiez le numéro d'étudiant.")
    except ValueError as e:
        print(e)
        return HTTPResponse(status=400, body="Erreur dans les données. Vérifiez le numéro d'étudiant.")
    except RuntimeError as e:
        print(e)
        return HTTPResponse(status=404, body=f"Impossible de récupérer l'emploi du temps pour <b>{student_id}</b>. Vérifiez le numéro d'étudiant.")

run(host='0.0.0.0', port=8080)
