"""
Author: Ivan Sabourin
"""

"""
# Only works on UNIX-like systems (GNU/Linux, MacOS, BSD)
"""
import requests
from math import inf
import subprocess
import merge_vcs
import VCalFormatter

def compareVersions(version1:str, version2:str)->bool:
    """
    version1 >= version2

    True if version1 is greater or equal to version2
    """
    v1numbers = [int(i) for i in version1.split(".")]
    v2numbers = [int(i) for i in version2.split(".")]
     
    for n1,n2 in zip(v1numbers, v2numbers):
        if n1>n2:
            return True
        elif n1<n2:
            return False
    return True

def getOSCommandOutput(command:str)->str:
    """
    Runs a command on the system and returns the output
    
    ## WARNING: The command will run on the system as-is, don't even think about passing a variable as a parameter.

    command: command to run
    
    output: stdout of the command
    """
    command = command.split(" ")
    result = subprocess.run(command, stdout=subprocess.PIPE)
    return result.stdout.decode("utf-8")

def assertNumber(numberToCheck:int, maxValue = inf)->None:
    """
    Asserts the variable passed is a valid number between 0 and the maximum

    Raises a ValueError if the variable isn't castable to int

    Raises an AssertionError if the variable isn't in the [0;max] range

    
    numberToCheck: alleged number

    maxValue (inf by default): maximum value allowed
    """
    try:
        digits = str(numberToCheck)

        # overkill digit-per-digit verification, just to be sure
        for d in digits:
            assert ord("0") <= ord(d) <= ord("9")
        assert 0<=numberToCheck<=maxValue
    except AssertionError:
        raise AssertionError (f"Attendu: int [0;{maxValue}]\nObtenu: {type(numberToCheck)}")

def assertHex(numberToCheck:str)->None:
    """
    Asserts the variable passed is a valid hexadecimal value

    Raises an AssertionError if the variable isn't a valid hexadecimal string

    numberToCheck: alleged hexadecimal value
    """
    try:
        for digit in numberToCheck:
            assert ((ord("0") <= ord(digit) <= ord("9"))or(ord("a") <= ord(digit) <= ord("f")))
    except AssertionError:
        raise AssertionError (f"Attendu: hexadécimal\nObtenu: {type(numberToCheck)}")

def getEDT(student_id:int, version:str)->str:
    """
    For a given student_id, returns the next two weeks of classes in the timetable.
    Week changes happen on saturdays, meaning while you're still in the working part of the week, the results will be for the current week and the next one, and when you reach saturday, it will roll over to the two next weeks.

    student_id: 6-digit student id ("N° étudiant")

    student_id is checked for integrity to avoid injection. Nothing guarenteed.

    Behaviour around new year might be messy, but will at worst not return results.
    """
    student_id = int(student_id)
    assertNumber(student_id, 999999)

    dayOfWeek = int(getOSCommandOutput("date +%u")[:-1])
    weekNumber = int(getOSCommandOutput("date +%W")[:-1])

    if dayOfWeek > 5:
        weekNumber = (weekNumber+1)%52 # modulo to 52 isn't correct every time but there are no classes on the last week (holidays)
        if weekNumber == 0:
            weekNumber = 1 # week numbers outrageously start at 1 for some incomprehensible, probably devilish, reason
    
    student_id = str(student_id)
    weekNumber = str(weekNumber)

    # 1 : Authentication

    url = "https://www.gpu-lr.fr/sat/index.php?page_param=accueilsatellys.php&cat=0&numpage=1&niv=0&clef=/"
    data = {
        "modeconnect": "connect",
        "util": student_id,
        "acct_pass": "123"
    }

    session = requests.Session()
    session.post(url, data=data, allow_redirects=True)

    # 2 : Intermediate step (ask Satellys, not me)
    
    url = "https://www.gpu-lr.fr/gpu/index.php"
    session.get(url)
    
    # 3 : Retrieve timetable for the next two weeks

    url = "https://www.gpu-lr.fr/gpu/gpu2vcs.php?semaine=" + weekNumber + "&prof_etu=ETU&etudiant=" + student_id + "&enseignantedt=%22,650,500,%22copyhistory=no,%20resizable=yes,menubar=yes,scrollbars=yes,statusbar=no%22"
    response = session.get(url)

    week1 = response.content.decode("utf-8")

    weekNumber = (int(weekNumber)+1)
    if weekNumber > 52:
        weekNumber = weekNumber%52+1
    weekNumber = str(weekNumber)

    url = "https://www.gpu-lr.fr/gpu/gpu2vcs.php?semaine=" + weekNumber + "&prof_etu=ETU&etudiant=" + student_id + "&enseignantedt=%22,650,500,%22copyhistory=no,%20resizable=yes,menubar=yes,scrollbars=yes,statusbar=no%22"
    response = session.get(url)

    week2 = response.content.decode("utf-8")

    # Merge next two weeks

    merged = merge_vcs.merge(week1, week2)

    # Version-dependant treatments
    if compareVersions(version, "1.2.0"):
        merged = VCalFormatter.formatDescription_1_2_0(merged)
    elif compareVersions(version, "1.1.0"):
        merged = VCalFormatter.formatSummary_1_1_0(merged)

    return merged
