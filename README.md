# C'est quoi ?

SatelliSync est serveur conçu pour récupérer votre emploi du temps et le rendre accessibles aux logiciels de calendrier tels que Google Calendar.

# Comment l'utiliser ?

## Démarrage rapide

- Rendez-vous à l'adresse [edt.midae.fr](https://edt.midae.fr)

- Complétez les informations

- Récupérez le lien obtenu et entrez-le dans votre logiciel de calendrier

## [Manuel complet](https://forge.iut-larochelle.fr/isabouri/gpu_extractor/-/wikis/home)

Contient les instructions complètes et l'aide pour utiliser SatelliSync, ainsi que les instructions pour héberger un serveur ou l'utiliser comme client direct vers GPU.
