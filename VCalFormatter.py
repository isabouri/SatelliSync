"""
Authors:
- Main author: Nicolas Foussard
- Minor modifications: Ivan Sabourin
"""

import quopri


def formatSummary_1_1_0(vCalendarStr:str)->str:
    """
    Edits the "summary" field to be more explicit

    Changes added by release 1.1.0

    Do not apply this function to a vcal that was already modified
    """
    # Transform string into list of lines
    lines = vCalendarStr.split("\n")

    # For each line in the VCAL string
    for i in range(len(lines)):

        # Detect lines that need to be edited (index i will refers to summary line and i-1 to description line)
        line = lines[i]
        if line.startswith("SUMMARY"):

            # Create list of elements in new summary
            newSummary = []
            newSummary.append(line[8:line.find("/")-1])                                         # Subject name
            newSummary.append(line[line.find("/")+2:line.find("/", line.find("/")+1)-1])        # Lesson type
            newSummary.append(lines[i+1][9:-1])                                                 # Room
            newSummary.append(lines[i-1][lines[i-1].find("Prof:")+5:lines[i-1].find("Spe:")-1]) # Teacher

            # Edit new lines
            lines[i] = "SUMMARY:" + " - ".join(filter(None, newSummary))

    return "\n".join(lines)

def formatDescription_1_2_0(vCalendarStr:str)->str:
    """
    Edits the "description" field to be more explicit, and applies formatting from previous versions
    
    Changes added by release 1.2.0

    Do not apply this function to a vcal that was already modified
    """
    vCalendarStr = formatSummary_1_1_0(vCalendarStr)
    # Transform string into list of lines
    lines = vCalendarStr.split("\n")

    # For each line in the VCAL string
    for i in range(len(lines)):

        # Detect lines that need to be edited (index i will refers to summary line and i-1 to description line)
        line = lines[i]
        if line.startswith("DESCRIPTION"):
            # Extract "Spe" field only
            descriptionLine = lines[i][lines[i].find("Spe:")+4:lines[i].find("\\")]
            # Decode quoted-printable to UTF-8
            descriptionLine = quopri.decodestring(descriptionLine)
            descriptionLine = descriptionLine.decode('utf-8')

            lines[i] = "DESCRIPTION;ENCODING=UTF-8:" + descriptionLine

    return "\n".join(lines)